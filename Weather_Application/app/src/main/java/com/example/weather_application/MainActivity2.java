package com.example.weather_application;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.BreakIterator;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class MainActivity2 extends AppCompatActivity {
    TextView NameOfCity, weatherState, Temperature, Country,windspeed,humidity;
    ImageView WeatherIcon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        weatherState = findViewById(R.id.weatherCondition);
        Temperature = findViewById(R.id.temperature);
        WeatherIcon = findViewById(R.id.weatherIcon);
        Country = findViewById(R.id.Country_name);
        windspeed=findViewById(R.id.Wind_speed);
        humidity=findViewById(R.id.humidity);
        String mCity = getIntent().getStringExtra("keyName");

        NameOfCity = findViewById(R.id.CityName);
        NameOfCity.setText(mCity);

    }


    public void get(View v) {
        final String API_Key = "5b6119d4b9172efed52118b172de195f";

        String mCity = NameOfCity.getText().toString();
        final String Weather_URl = "https://api.weatherstack.com/current?access_key=5b6119d4b9172efed52118b172de195f&query=" + mCity;
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Weather_URl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONObject object = response.getJSONObject("location");
                    JSONObject object1 = response.getJSONObject("current");
                    JSONObject object2 = response.getJSONObject("weather descriptions");
                    String humi = object.getString("humidity");
                    String wind = object.getString("wind_speed");
                    String temp = object.getString("temperature");
                    String city = object.getString("name");
                    String mCountry = object.getString("country");
                    NameOfCity.setText(city);
                    Country.setText(mCountry);
                    Temperature.setText(temp);
                    windspeed.setText(wind);
                    humidity.setText(humi);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity2.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        queue.add(request);

    }


}